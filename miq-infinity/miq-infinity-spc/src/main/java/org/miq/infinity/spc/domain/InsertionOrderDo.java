package org.miq.infinity.spc.domain;

public class InsertionOrderDo implements InsertionOrder {
	private int ioId;
	private String ioName;
	
	public int getIoId() {
		return ioId;
	}
	public void setIoId(int ioId) {
		this.ioId = ioId;
	}
	public String getIoName() {
		return ioName;
	}
	public void setIoName(String ioName) {
		this.ioName = ioName;
	}
	
	
}
