package org.miq.infinity.spc.engine;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.miq.infinity.spc.domain.SPCRecommendationsAndTradersInputDo;
import org.miq.infinity.spc.domain.SpcGeneratedRecommendationsDo;
import org.miq.infinity.spc.domain.SpcRecommendationDo;
import org.miq.infinity.spc.domain.TraderInputCriteriaImpl;
import org.miq.infinity.spc.entity.CommercialInputStore;
import org.miq.infinity.spc.entity.SpcComputationOutputStore;
import org.miq.infinity.spc.exception.ResourceNotFoundException;
import org.miq.infinity.spc.repository.CommercialInputStoreDao;
import org.miq.infinity.spc.repository.SpcComputationOutputRepository;
import org.miq.infinity.spc.util.PythonScriptExecutor;
import org.miq.infinity.spc.util.SpcUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Transactional
@Service
public class SpcRecommendationOutputService {

    private static final String MEDIA_OWNER_PRIORITIES_NOT_AVAILABLE = "Media Owner Priorities are not available.Please upload file in required format.";

	private static final Logger LOG = LoggerFactory.getLogger(SpcRecommendationOutputService.class);

    @Autowired
    CommercialMediaOwnersService spcCommercialService;

    @Autowired
    CommercialInputStoreDao commercialInputStoreDao;

    @Autowired
    private SpcComputationOutputRepository spcComputationOutputRepository;

    @Autowired
    private Environment environment; 

    @Autowired
    private SpcUtil spcUtil;

    public SPCRecommendationsAndTradersInputDo generateSPCRecommendations(TraderInputCriteriaImpl traderInputCriteria, int profileId) {
    	SPCRecommendationsAndTradersInputDo sPCRecommendationsAndTradersInputDo = null;
    	List<SpcRecommendationDo> generatedSpcRecommendation = null;
    	String commInputToJson = null;
        String traderInputToJson = null;
        String spcOutput = null;
        String pythonScriptPath = null;
        String pythonScriptName = environment.getProperty("commercial.python.script.name");
        pythonScriptPath = getSPCGeneratingScriptPath(pythonScriptName);
        LOG.info("SPC Recommendation Generating script URL: {}",pythonScriptPath);
        ObjectMapper objectMapper = new ObjectMapper();
        List<CommercialInputStore> list = spcCommercialService.getMediaOwnersAndAssociatedPriorities();

        if (spcUtil.isCollectionEmpty(list)) {
            throw new ResourceNotFoundException(MEDIA_OWNER_PRIORITIES_NOT_AVAILABLE);
        } 
        try {
            commInputToJson = objectMapper.writeValueAsString(list);
            traderInputToJson = objectMapper.writeValueAsString(traderInputCriteria);

            LOG.info("Media Owner Priorities List required for generating SPC Recommendation is {}", commInputToJson);
            LOG.info("Trader input for spc computation is {}", traderInputToJson);
            String[] criteriaArgs = prepareSPCRecommendationGeneratorCriteria(pythonScriptPath,commInputToJson,traderInputToJson);
            spcOutput = PythonScriptExecutor.executeScript(criteriaArgs);
            //spcOutput = spcUtil.computeSpcAlgorithm(commInputToJson, traderInputToJson, pythonScriptPath);
            LOG.info("SPC Recommendation output  is : {}" , spcOutput);
            if (spcOutput == null){
                throw new ResourceNotFoundException("Error: SPC recommendations could not be generated.");
            }
           
          generatedSpcRecommendation = parseAndSaveSpcOutput(spcOutput, profileId, traderInputCriteria);
          SpcGeneratedRecommendationsDo spcGeneratedRecommendationsDo = new SpcGeneratedRecommendationsDo();
          spcGeneratedRecommendationsDo.setSpcRecommendations(generatedSpcRecommendation);
          
          sPCRecommendationsAndTradersInputDo = new SPCRecommendationsAndTradersInputDo();
          sPCRecommendationsAndTradersInputDo.setSpcRecommendations(generatedSpcRecommendation);
          sPCRecommendationsAndTradersInputDo.setTraderInput(traderInputCriteria);
          
          if(!generatedSpcRecommendation.isEmpty() && generatedSpcRecommendation != null) {
        	  LOG.info("Successfully saved SPC Output and retrieved active list of recommendations as {}",generatedSpcRecommendation.toString());
          }else{
        	  LOG.error("Some issue while generating Recommendations");
          }
        } catch (JsonProcessingException e1) {
            LOG.error("Inside SpcRecommendationOutputService - generateSPCRecommendations(): Error in generating spc recommendation :: {}", e1);
        } catch (ResourceNotFoundException e2) {
            LOG.error("Computation output Not found (Inside SpcRecommendationOutputService):: {}",e2);
            throw new ResourceNotFoundException(e2.getMessage());
        } 
        return sPCRecommendationsAndTradersInputDo;
    }

	private String getSPCGeneratingScriptPath(String pythonScriptName) {
		String pythonScriptPath;
		//ClassLoader classLoader = new SpcRecommendationOutputService().getClass().getClassLoader();
		ClassLoader classLoader = SpcRecommendationOutputService.class.getClassLoader();
        File file = new File(classLoader.getResource(pythonScriptName).getFile());
        pythonScriptPath = file.toPath().toString();
		return pythonScriptPath;
	}

	private String[] prepareSPCRecommendationGeneratorCriteria(String pythonScriptPath,String commInputToJson,String traderInputToJson) {
    	String[] args = new String[4];
    	args[0] = "python";
    	args[1] = pythonScriptPath;
    	args[2] = commInputToJson;
    	args[3] = traderInputToJson;
		
		return args;
	}

	public List<SpcRecommendationDo> parseAndSaveSpcOutput(String spcOutput, int profileId, TraderInputCriteriaImpl traderinputcriteria) {
		
		//Gson gson = new Gson();
		//SpcComputationOutputCriteriaImpl spcOutputObj = gson.fromJson (spcOutput, SpcComputationOutputCriteriaImpl.class);
		List<SpcRecommendationDo> parsedSPCOutputlist = new ArrayList<>();
        int isActive = 0;
        int updatedRecommendationId = 0;

        updateExistingRecord(isActive);

        String recommendationId = spcComputationOutputRepository.getRecommendationIdentifier();

        if (recommendationId == null) {
            updatedRecommendationId = 1;

        } else {
            updatedRecommendationId = Integer.parseInt(recommendationId);
            updatedRecommendationId = updatedRecommendationId + 1;
        }

        LOG.info(" Created Unique identifier to identify and group Generated SPC Recommendation as per user {}", updatedRecommendationId);

        parsedSPCOutputlist = spcComputationOutputRepository.saveSpcOutput(spcOutput, profileId, isActive, traderinputcriteria,
                updatedRecommendationId);
        LOG.info("Parsed SPC output is :: {}", parsedSPCOutputlist.toString());

        return parsedSPCOutputlist;
    }

    public List<SpcComputationOutputStore> getAllSpcOutput(int isActive) {
        return spcComputationOutputRepository.getAllSpcOutputList(isActive);
    }

    public void updateExistingRecord(int isActive) {
        spcComputationOutputRepository.updateSpcOutput(isActive);
    }
}
