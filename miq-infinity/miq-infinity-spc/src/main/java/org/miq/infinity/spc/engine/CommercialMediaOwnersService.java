package org.miq.infinity.spc.engine;

import java.sql.SQLException;
import java.util.List;

import org.miq.infinity.spc.domain.CommInputCreteriaImpl;
import org.miq.infinity.spc.domain.DeleteMediaOwner;
import org.miq.infinity.spc.domain.MediaOwner;
import org.miq.infinity.spc.domain.MediaOwners;
import org.miq.infinity.spc.entity.CommercialInputStore;
import org.miq.infinity.spc.exception.BadRequestException;
import org.miq.infinity.spc.repository.CommercialMediaOwnersRepository;
import org.miq.infinity.spc.util.SpcUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;

@Transactional
@Service
public class CommercialMediaOwnersService {

    private static final Logger LOG = LoggerFactory.getLogger(CommercialMediaOwnersService.class);

    @Autowired
    SpcProfileService spcProfileService;

    @Autowired
    SpcUtil spcUtil;
 
    @Autowired
    CommercialMediaOwnersRepository commercialInputRepository;
 
    public List<CommercialInputStore> getMediaOwnersAndAssociatedPriorities() {
        return commercialInputRepository.getMediaOwnersAndAssociatedPriorities();
     
    }
    
    public List<MediaOwner> getMediaOwnersLookupData() throws SQLException {
        return commercialInputRepository.getDbmMediaOwnersFromRedshiftLookup();
    }
    
    
 
    public MediaOwners uploadMediaOwnersCommercialFileData(MultipartFile uploadfile, String profileName) throws Exception {
        String status = "";

        MediaOwners mediaOwnersAndPrioritiesObj = null;
        MediaOwners mediaOwnersAndPrioritiesObjSavedToDB = null;
        String mediaOwnersAndPriorities = null;
        int profileId = spcProfileService.getProfileId(profileName);
        LOG.info("Saved profile for {}:: in database with id {}:: ", profileName);
        mediaOwnersAndPrioritiesObj = spcUtil.parseFileAndreturnCommercialInputsList(uploadfile, profileId);
        mediaOwnersAndPriorities = mediaOwnersAndPrioritiesObj.toString();
            LOG.info("Media Owner Priorities File Data {}", mediaOwnersAndPriorities);

            if (!spcUtil.isCollectionEmpty(mediaOwnersAndPrioritiesObj.getMediaOwners())) {
                // Calling function to save data in to table
            	mediaOwnersAndPrioritiesObjSavedToDB = commercialInputRepository.saveMediaOwnersAndAssociatedPriorities(mediaOwnersAndPrioritiesObj,profileId);
                LOG.info("Media Owner Priorities data saved to DB. {}", mediaOwnersAndPriorities);
            } else {
                status = "Error in uploading Media Owners and Associated Priorities File";
                LOG.error("Error in uploading Media Owners and Associated Priorities File");
                throw new BadRequestException("Please upload file with valid content");
            }
        return mediaOwnersAndPrioritiesObjSavedToDB;
    }
 
    public String updateCommercialInput(CommInputCreteriaImpl commInputCriteria) throws JsonProcessingException {
        return commercialInputRepository.updateCommercialRecord(commInputCriteria);
    }

    public DeleteMediaOwner deleteMediaOwner(int id) {
        return commercialInputRepository.deleteMediaOwner(id);
    }
}
