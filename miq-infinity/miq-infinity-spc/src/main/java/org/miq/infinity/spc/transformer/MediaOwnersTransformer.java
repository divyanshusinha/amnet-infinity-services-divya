package org.miq.infinity.spc.transformer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.miq.infinity.spc.domain.MediaOwner;
import org.miq.infinity.spc.domain.MediaOwners;
import org.miq.infinity.spc.domain.MediaOwnersDo;
import org.miq.infinity.spc.entity.CommercialInputStore;
import org.python.jline.internal.Log;

public class MediaOwnersTransformer {
	
	public static List<CommercialInputStore> transform(MediaOwners mediaOwnersAndAssociatedPriorities,int profileId){
		Date currentDate = new Date();
		List<CommercialInputStore> commercialInputsStore = new ArrayList<>();
		CommercialInputStore commercialInputStore = null;
		Log.debug("Transforming DAO to desired obj",mediaOwnersAndAssociatedPriorities.getMediaOwners().toString());
		List<MediaOwner> mediaOwners = new ArrayList<>();
		MediaOwnersDo mediaOwnersDo = new MediaOwnersDo(); 
		if(!mediaOwnersAndAssociatedPriorities.getMediaOwners().isEmpty()) {
			for(MediaOwner mediaOwnerObj : mediaOwnersAndAssociatedPriorities.getMediaOwners()) {
				commercialInputStore = new CommercialInputStore();
				commercialInputStore.setMediaOwner(mediaOwnerObj.getMediaOwner());
				commercialInputStore.setPriority(mediaOwnerObj.getPriority());
				commercialInputStore.setCreateDate(currentDate);
				commercialInputStore.setUpdateDate(currentDate);
				if(mediaOwnerObj.getPriority() == 0) {
					commercialInputStore.setIsActive(0);	
				}else{
					commercialInputStore.setIsActive(1);
				}
				commercialInputStore.setProfileId(profileId);
				commercialInputsStore.add(commercialInputStore);
			}
		}else {
			Log.info("Media Owner list passed to transformer is empty");
		}
		mediaOwnersDo.setMediaOwners(mediaOwners);
		return commercialInputsStore;
	}
}
