package org.miq.infinity.spc.domain;

import java.util.Date;
import java.util.List;

public class TraderInputCriteriaImpl implements TraderInputCriteria {
	
	private int advertiserId;
	private int insertionOrderId; 
    private int campaignId;
    private String profileName;
    private String campaignType;
    private Date startdate;
    private Date enddate;
    private long impressions;
    private long budget;
    private String ctrGoal;
    private String viewabilityGoal;
    //private List<String> deviceType;
    private String deviceType;
    private List<String> creativeSize;

    public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public List<String> getCreativeSize() {
        return creativeSize;
    }

    public void setCreativeSize(List<String> creativeSize) {
        this.creativeSize = creativeSize;
    }

    public String getCampaignType() {
        return campaignType;
    }

    public void setCampaignType(String campaignType) {
        this.campaignType = campaignType;
    }

    public void setAdvertiserId(int advertiserId) {
        this.advertiserId = advertiserId;

    }

    public void setCampaignId(int campaignId) {
        this.campaignId = campaignId;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }

    public void setImpressions(long impressions) {
        this.impressions = impressions;
    }

    public void setBudget(long budget) {
        this.budget = budget;
    }

    

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public void setCtrGoal(String ctrGoal) {
        this.ctrGoal = ctrGoal;
    }

    public void setViewabilityGoal(String viewabilityGoal) {
        this.viewabilityGoal = viewabilityGoal;
    }

    @Override
    public int getAdvertiserId() {
        return advertiserId;
    }

    @Override
    public int getCampaignId() {
        return campaignId;
    }

    @Override
    public Date getStartdate() {
        return startdate;
    }

    @Override
    public Date getEnddate() {
        return enddate;
    }

    @Override
    public long getImpressions() {
        return impressions;
    }

    @Override
    public long getBudget() {
        return budget;
    }

    @Override
    public String getCtrGoal() {
        return ctrGoal;
    }

    @Override
    public String getViewabilityGoal() {
        return viewabilityGoal;
    }

  
    @Override
    public String getProfileName() {
        return profileName;
    }

//	public List<String> getDeviceType() {
//		return deviceType;
//	}
//
//	public void setDeviceType(List<String> deviceType) {
//		this.deviceType = deviceType;
//	}

 

	public int getInsertionOrderId() {
		return insertionOrderId;
	}

	public void setInsertionOrderId(int insertionOrderId) {
		this.insertionOrderId = insertionOrderId;
	}

	@Override
	public String getDeviceType() {
		// TODO Auto-generated method stub
		return this.deviceType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + advertiserId;
		result = prime * result + (int) (budget ^ (budget >>> 32));
		result = prime * result + (int) (campaignId ^ (campaignId >>> 32));
		result = prime * result + ((campaignType == null) ? 0 : campaignType.hashCode());
		result = prime * result + ((creativeSize == null) ? 0 : creativeSize.hashCode());
		result = prime * result + ((ctrGoal == null) ? 0 : ctrGoal.hashCode());
		result = prime * result + ((deviceType == null) ? 0 : deviceType.hashCode());
		result = prime * result + ((enddate == null) ? 0 : enddate.hashCode());
		result = prime * result + (int) (impressions ^ (impressions >>> 32));
		result = prime * result + insertionOrderId;
		result = prime * result + ((profileName == null) ? 0 : profileName.hashCode());
		result = prime * result + ((startdate == null) ? 0 : startdate.hashCode());
		result = prime * result + ((viewabilityGoal == null) ? 0 : viewabilityGoal.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TraderInputCriteriaImpl other = (TraderInputCriteriaImpl) obj;
		if (advertiserId != other.advertiserId)
			return false;
		if (budget != other.budget)
			return false;
		if (campaignId != other.campaignId)
			return false;
		if (campaignType == null) {
			if (other.campaignType != null)
				return false;
		} else if (!campaignType.equals(other.campaignType))
			return false;
		if (creativeSize == null) {
			if (other.creativeSize != null)
				return false;
		} else if (!creativeSize.equals(other.creativeSize))
			return false;
		if (ctrGoal == null) {
			if (other.ctrGoal != null)
				return false;
		} else if (!ctrGoal.equals(other.ctrGoal))
			return false;
		if (deviceType == null) {
			if (other.deviceType != null)
				return false;
		} else if (!deviceType.equals(other.deviceType))
			return false;
		if (enddate == null) {
			if (other.enddate != null)
				return false;
		} else if (!enddate.equals(other.enddate))
			return false;
		if (impressions != other.impressions)
			return false;
		if (insertionOrderId != other.insertionOrderId)
			return false;
		if (profileName == null) {
			if (other.profileName != null)
				return false;
		} else if (!profileName.equals(other.profileName))
			return false;
		if (startdate == null) {
			if (other.startdate != null)
				return false;
		} else if (!startdate.equals(other.startdate))
			return false;
		if (viewabilityGoal == null) {
			if (other.viewabilityGoal != null)
				return false;
		} else if (!viewabilityGoal.equals(other.viewabilityGoal))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TraderInputCriteriaImpl [advertiserId=" + advertiserId + ", insertionOrderId=" + insertionOrderId
				+ ", campaignId=" + campaignId + ", profileName=" + profileName + ", campaignType=" + campaignType
				+ ", startdate=" + startdate + ", enddate=" + enddate + ", impressions=" + impressions + ", budget="
				+ budget + ", ctrGoal=" + ctrGoal + ", viewabilityGoal=" + viewabilityGoal + ", deviceType="
				+ deviceType + ", creativeSize=" + creativeSize + "]";
	}

	
 
}
