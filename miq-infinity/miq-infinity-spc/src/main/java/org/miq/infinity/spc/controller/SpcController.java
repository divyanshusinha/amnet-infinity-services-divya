package org.miq.infinity.spc.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.miq.infinity.spc.domain.Advertisers;
import org.miq.infinity.spc.domain.Campaigns;
import org.miq.infinity.spc.domain.CommInputCreteriaImpl;
import org.miq.infinity.spc.domain.DeleteMediaOwner;
import org.miq.infinity.spc.domain.InsertionOrders;
import org.miq.infinity.spc.domain.MediaOwner;
import org.miq.infinity.spc.domain.MediaOwners;
import org.miq.infinity.spc.domain.SPCRecommendationsAndTradersInputDo;
import org.miq.infinity.spc.domain.TraderInputCriteriaImpl;
import org.miq.infinity.spc.engine.CommercialMediaOwnersService;
import org.miq.infinity.spc.engine.DbmAdvertiserIoCampaignDataService;
import org.miq.infinity.spc.engine.SpcRecommendationOutputService;
import org.miq.infinity.spc.engine.SpcTraderService;
import org.miq.infinity.spc.entity.CommercialInputStore;
import org.miq.infinity.spc.entity.SpcComputationOutputStore;
import org.miq.infinity.spc.exception.ResourceNotFoundException;
import org.miq.infinity.spc.repository.CommercialInputStoreDao;
import org.miq.infinity.spc.util.SpcUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;

import io.swagger.annotations.ApiOperation;

@RestController
@CrossOrigin
@RequestMapping("/supply-partner-controls")
public class SpcController {

    private static final Logger LOG = LoggerFactory.getLogger(SpcController.class);

    @Autowired
    private CommercialMediaOwnersService commercialMediaOwnersService;

    @Autowired
    private SpcRecommendationOutputService spcRecommendationOutputService;

    @Autowired
    private SpcTraderService spcTraderService;
    
    @Autowired
    private DbmAdvertiserIoCampaignDataService dbmAdvertiserIoCampaignDataService;
    
    @Autowired 
    SpcUtil spcUtil;

    @Autowired
    CommercialInputStoreDao commercialInputStoreDao;

    static final class Uri {
        private Uri() {
        }
        public static final String MEDIA_OWNER_PRIORITIES = "/media-owners/priorities";
        public static final String MEDIA_OWNER_PRIORITIES_FILE_UPLOAD = "/media-owners/upload";
        public static final String MEDIA_OWNER = "/media-owner";
        public static final String CAMPAIGN_RECOMMENDATION_PROVIDER = "/campaign-recommendation-provider";
        public static final String CAMPAIGN_RECOMMENDATIONS = "/campaign-recommendations";
        public static final String MEDIA_OWNERS_LOOKUP ="/media-owners-lookup";
        public static final String DBM_ADVERTISERS_LOOKUP="/dbm-advertisers";
        public static final String DBM_IO_LOOKUP="/dbm-insertion-orders";
        public static final String DBM_CAMPAIGNS_LOOKUP="/dbm-campaigns";
    }
    
    @ApiOperation(value = "This API consumes excel file(with columns 'Media Owners' and 'Priorities') which contains media owners and priorities.")
    @RequestMapping(value = Uri.MEDIA_OWNER_PRIORITIES_FILE_UPLOAD, headers = "Content-Type= multipart/form-data", method = RequestMethod.POST,produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<MediaOwners> uploadMediaOwnersCommercialFile(@RequestParam("file") MultipartFile uploadFile,@RequestParam("profilename") String profileName) throws Exception {
        ResponseEntity<MediaOwners> response = null;
            LOG.info("Uploading commercial input {} file", uploadFile.getOriginalFilename());
            MediaOwners mediaOwners = commercialMediaOwnersService.uploadMediaOwnersCommercialFileData(uploadFile, profileName);
            if(mediaOwners != null) {
            	response = new ResponseEntity<>(mediaOwners, HttpStatus.OK);
            	return response;
            }else {
            	response = new ResponseEntity<>(mediaOwners , HttpStatus.BAD_REQUEST);
            }
        
        LOG.info("Exiting SpcController after uploading {} commercial input: {} ", uploadFile.getOriginalFilename());
        return response;
    }
    
    @ApiOperation(value = "This API returns list of media owners with priorities associated with them.")
    @RequestMapping(value = Uri.MEDIA_OWNER_PRIORITIES, method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<List<CommercialInputStore>> getMediaOwnersAndPriorities() {
        ResponseEntity<List<CommercialInputStore>> responseEntity;
        List<CommercialInputStore> mediaOwnersAndPrioritiesList=null;
        LOG.debug("Inside SPCController : getMediaOwnersAndPriorities()");
        mediaOwnersAndPrioritiesList = commercialMediaOwnersService.getMediaOwnersAndAssociatedPriorities();
        if (spcUtil.isCollectionEmpty(mediaOwnersAndPrioritiesList)){
           throw new ResourceNotFoundException("Media Owner priority list not found, Please upload required file first");
        }
        System.out.println("::::::::::::::Outside empty");
        responseEntity= new ResponseEntity<>(mediaOwnersAndPrioritiesList, HttpStatus.OK);
        LOG.info("Exiting SpcController after listing commercial input");
      
        return responseEntity;
    }  
    
    @ApiOperation(value = "This API updates single record which contains media owner and priority.")
    @RequestMapping(value = Uri.MEDIA_OWNER, method = RequestMethod.PUT, consumes = {MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<String> updateMediaOwner(@RequestBody CommInputCreteriaImpl commInputCriteria) throws JsonProcessingException{
        ResponseEntity<String> response = null;
        String updatedRecord = null;
            LOG.info("updating commercial input for {} ", commInputCriteria.getDomain());
            updatedRecord = commercialMediaOwnersService.updateCommercialInput(commInputCriteria);
        response = new ResponseEntity<>(updatedRecord + "-", HttpStatus.OK);
        return response;
    }

    @ApiOperation(value = "This API deletes single record which contains media owner and priority.")
    @RequestMapping(value = Uri.MEDIA_OWNER+"/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<DeleteMediaOwner> deleteMediaOwner(@PathVariable("id") int id) {

        ResponseEntity<DeleteMediaOwner> response = null;
        DeleteMediaOwner deleteRecord = null;
            LOG.info("deleting commercial input for {} ", id);
            deleteRecord = commercialMediaOwnersService.deleteMediaOwner(id);
        response = new ResponseEntity<>(deleteRecord, HttpStatus.OK);
        return response;
    }

    @ApiOperation(value = "This api takes Traders inputs and provides campaign recommendations")
    @RequestMapping(value = Uri.CAMPAIGN_RECOMMENDATION_PROVIDER, method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<SPCRecommendationsAndTradersInputDo> processTraderInputsAndGenerateCampaignRecommendation(@RequestBody TraderInputCriteriaImpl traderinputcriteria) throws IOException {
        ResponseEntity<SPCRecommendationsAndTradersInputDo> response = null;
        SPCRecommendationsAndTradersInputDo sPCRecommendationsAndTradersInputDo = null;
        LOG.info("Processing Traders input with Username:", traderinputcriteria.getProfileName());
        sPCRecommendationsAndTradersInputDo = spcTraderService.processTraderInput(traderinputcriteria);
        if(sPCRecommendationsAndTradersInputDo == null) {
        	 throw new ResourceNotFoundException("Server error: Could not generate recommendations");
        }
        response = new ResponseEntity<>(sPCRecommendationsAndTradersInputDo, HttpStatus.OK); 
        return response;
    }

    @ApiOperation(value = "This API returns list of latest campaign recommendation generated by trader.")
    @RequestMapping(value = Uri.CAMPAIGN_RECOMMENDATIONS, method = RequestMethod.GET, produces = {
            MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<List<SpcComputationOutputStore>> getAllSpcRecommendations() {
        int isActive = 1;
        LOG.info("Getting all spc computation");
        return new ResponseEntity<>(
                spcRecommendationOutputService.getAllSpcOutput(isActive), HttpStatus.OK);
    }
    
    @ApiOperation(value = "This API returns list of media owners from lookup table present in Amnet Data Redshift Cluster.")
    @RequestMapping(value = Uri.MEDIA_OWNERS_LOOKUP, method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<List<MediaOwner>> getMediaOwnersFromLookup() throws SQLException {
        ResponseEntity<List<MediaOwner>> responseEntity;
        List<MediaOwner> mediaOwnersList=null;
        LOG.debug("Inside getMediaOwnersFromLookup() to fetch MediaOwners From Lookup");
        mediaOwnersList = commercialMediaOwnersService.getMediaOwnersLookupData();
        if (spcUtil.isCollectionEmpty(mediaOwnersList)){
            throw new ResourceNotFoundException("Media Owner lookup data not found in Redshift");
        }
        responseEntity= new ResponseEntity<>(mediaOwnersList, HttpStatus.OK);
        LOG.debug("Exiting getMediaOwnersFromLookup() after fetching MediaOwners From Lookup");
      
        return responseEntity;
    }  
    
    @ApiOperation(value = "This API returns list of DBM Advertisers from lookup table present in Amnet Reports Redshift Cluster.")
    @RequestMapping(value = Uri.DBM_ADVERTISERS_LOOKUP, method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<Advertisers> getDBMadvertisers(){
        ResponseEntity<Advertisers> responseEntity;
        Advertisers  advertisersList=null;
        LOG.debug("Inside advertisersList() to fetch DBM Advertisers From Lookup");
        advertisersList = dbmAdvertiserIoCampaignDataService.getDbmAdvertisers();
        if (spcUtil.isCollectionEmpty(advertisersList.getAdvertisers())){
            throw new ResourceNotFoundException("DBM Advertisers lookup data not found in Redshift");
        }
        responseEntity= new ResponseEntity<>(advertisersList, HttpStatus.OK);
        LOG.debug("Exiting getDBMadvertisers() after fetching MediaOwners From Lookup");
      
        return responseEntity;
    }  
    
    
    @ApiOperation(value = "This API returns list of DBM Insertion Orders for particular advertiser from lookup table present in Amnet Reports Redshift Cluster.")
    @RequestMapping(value = Uri.DBM_IO_LOOKUP, method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<InsertionOrders> getDbmInsertionOrders(@RequestParam("advertiserId") int advertiserId){
        ResponseEntity<InsertionOrders> responseEntity;
        InsertionOrders  insertionOrdersList=null;
        LOG.info("Inside getDbmInsertionOrders() to fetch DBM InsertionOrders From Lookup");
        insertionOrdersList = dbmAdvertiserIoCampaignDataService.getDbmInsertionOrdersByAdvertiserId(advertiserId);
        if (spcUtil.isCollectionEmpty(insertionOrdersList.getInsertionOrders())){
            throw new ResourceNotFoundException("Insertion Orders data not found.");
        }
        responseEntity= new ResponseEntity<>(insertionOrdersList, HttpStatus.OK);
        LOG.info("Exiting getDbmInsertionOrders() after fetching insertion orders From Lookup");
      
        return responseEntity;
    }  
    
    
    @ApiOperation(value = "This API returns list of DBM Insertion Orders for particular advertiser from lookup table present in Amnet Reports Redshift Cluster.")
    @RequestMapping(value = Uri.DBM_CAMPAIGNS_LOOKUP, method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<Campaigns> getDbmCampaignByInsertionOrder(@RequestParam("insertionOrderId") int insertionOrderId){
        ResponseEntity<Campaigns> responseEntity;
        Campaigns campaignsList = null;
        LOG.debug("Inside getDbmInsertionOrders() to fetch DBM InsertionOrders From Lookup");
        campaignsList = dbmAdvertiserIoCampaignDataService.getDbmCampaignByInsertionOrderId(insertionOrderId);
        if (spcUtil.isCollectionEmpty(campaignsList.getCampaigns())){
            throw new ResourceNotFoundException("Campaigns for insertion order is not found.");
        }
        responseEntity= new ResponseEntity<>(campaignsList, HttpStatus.OK);
        LOG.debug("Exiting getDbmCampaignByInsertionOrder() after fetching Campaigns From Lookup");
      
        return responseEntity;
    }  
    
    
    

    

}