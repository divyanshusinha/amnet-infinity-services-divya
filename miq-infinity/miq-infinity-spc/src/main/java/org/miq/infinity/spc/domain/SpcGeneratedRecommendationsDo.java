package org.miq.infinity.spc.domain;

import java.util.List;

public class SpcGeneratedRecommendationsDo implements SpcGeneratedRecommendations {
	List<SpcRecommendationDo> spcRecommendations;

	public List<SpcRecommendationDo> getSpcRecommendations() {
		return spcRecommendations;
	}

	public void setSpcRecommendations(List<SpcRecommendationDo> spcRecommendations) {
		this.spcRecommendations = spcRecommendations;
	}

	@Override
	public String toString() {
		return "SpcGeneratedRecommendationsDo [spcRecommendations=" + spcRecommendations + "]";
	}
	
	
}