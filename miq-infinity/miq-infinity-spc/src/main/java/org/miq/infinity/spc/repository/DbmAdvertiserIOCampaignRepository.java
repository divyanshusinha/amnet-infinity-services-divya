package org.miq.infinity.spc.repository;

import org.miq.infinity.spc.domain.Advertisers;
import org.miq.infinity.spc.domain.Campaigns;
import org.miq.infinity.spc.domain.InsertionOrders;

public interface DbmAdvertiserIOCampaignRepository {
	public Advertisers getDbmAdvertisers();
	public InsertionOrders getDbmInsertionOrdersByAdvertiserId(int advertiserId);
	public Campaigns getDbmCampaignByInsertionOrderId(int insertionOrderId);
}
