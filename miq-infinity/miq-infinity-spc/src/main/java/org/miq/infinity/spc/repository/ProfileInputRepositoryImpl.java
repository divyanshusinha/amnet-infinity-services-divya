package org.miq.infinity.spc.repository;

import org.miq.infinity.spc.entity.ProfileInputStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ProfileInputRepositoryImpl implements ProfileInputRepository {

    @Autowired
    private ProfileInputStoreDao profileInputStoreDao;

    private static final Logger LOG = LoggerFactory.getLogger(ProfileInputRepositoryImpl.class);

    @Override
    public int getAndSaveProfile(String profileName) {

        int profileId = 0;
        ProfileInputStore profileInput = new ProfileInputStore();
        try {
            Object object = profileInputStoreDao.findByprofileName(profileName);

            if (object == null) {
                profileInput.setProfileName(profileName);
                profileInputStoreDao.save(profileInput);
                profileId = profileInput.getProfileId();
                LOG.info("profielId is {}", profileId);
            } else {
                profileInput = (ProfileInputStore) object;
                profileId = profileInput.getProfileId();
                LOG.info("profielId is {}", profileId);
            }
        } catch (Exception e) {
            LOG.error("Unable to get profile id for {}", profileName);
            throw e;
        }
        return profileId;
    }

}
