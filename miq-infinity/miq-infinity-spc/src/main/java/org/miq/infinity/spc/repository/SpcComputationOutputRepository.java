package org.miq.infinity.spc.repository;

import java.util.List;

import org.miq.infinity.spc.domain.SpcRecommendationDo;
import org.miq.infinity.spc.domain.TraderInputCriteriaImpl;
import org.miq.infinity.spc.entity.SpcComputationOutputStore;

public interface SpcComputationOutputRepository {

    public String getRecommendationIdentifier();

    public void updateSpcOutput(int isActive);

    public List<SpcComputationOutputStore> getAllSpcOutputList(int isActive);

    public List<SpcRecommendationDo> saveSpcOutput(String spcOutput, int profileId, int isActive,
            TraderInputCriteriaImpl traderinputcriteria, int updatedRecommendationId);

}
