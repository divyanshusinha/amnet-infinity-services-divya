package org.miq.infinity.spc.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class DataBaseAccessException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public DataBaseAccessException(String message) {
        super(message);

    }

    public DataBaseAccessException(String message, Throwable cause) {
        super(message);

    }

}
