package org.miq.infinity.spc.repository;

import org.miq.infinity.spc.domain.TraderInputCriteriaImpl;
import org.miq.infinity.spc.engine.SpcProfileService;
import org.miq.infinity.spc.entity.TraderInputStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class TraderInputRepositoryImpl implements TraderInputRepository {

    private static final Logger LOG = LoggerFactory.getLogger(TraderInputRepositoryImpl.class);
    private static final String DATA_SUCCESS = "Data saved successfully";
    private static final String DATA_ERROR = "Data not saved";

    @Autowired
    SpcProfileService spcProfileService;

    @Autowired
    private TraderInputStoreDao traderInputStoreDao;

    @Override
    public String saveTraderInput(TraderInputCriteriaImpl traderInputCriteria, int profileId) {

        String traderInput = null;
        TraderInputStore traderInputStore = new TraderInputStore();
        BeanUtils.copyProperties(traderInputCriteria, traderInputStore);
        traderInputStore.setProfileId(profileId);
        traderInputStore.setCreativeSize(traderInputCriteria.getCreativeSize().toString());
        traderInputStore.setInsertionOrderId(traderInputCriteria.getInsertionOrderId());
        traderInputStoreDao.save(traderInputStore);
        LOG.info("Saving Trader Input {}",traderInputStore.toString());
        traderInput = traderInputCriteria.toString();
        if (traderInput == null) {
            LOG.error("Error in saving trader input to databases table {}::", traderInput);
            return DATA_ERROR;
        }
        return DATA_SUCCESS;
    }
}
