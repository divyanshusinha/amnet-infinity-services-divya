package org.miq.infinity.spc.domain;

import java.util.List;

public class CampaignsDo implements Campaigns{

	List<Campaign> campaigns;

	public List<Campaign> getCampaigns() {
		return campaigns;
	}

	public void setCampaigns(List<Campaign> campaigns) {
		this.campaigns = campaigns;
	}
	
	
}