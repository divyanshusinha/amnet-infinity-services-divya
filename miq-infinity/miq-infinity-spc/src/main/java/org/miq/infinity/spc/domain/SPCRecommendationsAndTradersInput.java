package org.miq.infinity.spc.domain;

import java.util.List;

public interface SPCRecommendationsAndTradersInput {
	TraderInputCriteria getTraderInput();
	List<SpcRecommendationDo> getSpcRecommendations();
}
