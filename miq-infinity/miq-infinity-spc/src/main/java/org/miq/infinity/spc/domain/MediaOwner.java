package org.miq.infinity.spc.domain;

public interface MediaOwner {
	String getMediaOwner();
	int getPriority();
}
