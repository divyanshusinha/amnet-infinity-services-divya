package org.miq.infinity.spc.repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.miq.infinity.spc.domain.CommInputCreteriaImpl;
import org.miq.infinity.spc.domain.DeleteMediaOwner;
import org.miq.infinity.spc.domain.DeleteMediaOwnerDo;
import org.miq.infinity.spc.domain.MediaOwner;
import org.miq.infinity.spc.domain.MediaOwnerDo;
import org.miq.infinity.spc.domain.MediaOwners;
import org.miq.infinity.spc.entity.CommercialInputStore;
import org.miq.infinity.spc.exception.DataBaseAccessException;
import org.miq.infinity.spc.redshiftmanager.RedshiftConnectionProvider;
import org.miq.infinity.spc.redshiftmanager.RedshiftQueryExecutor;
import org.miq.infinity.spc.transformer.MediaOwnersTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Repository
@PropertySource("classpath:redshift-db.properties")
public class CommercialMediaOwnersRepositoryImpl implements CommercialMediaOwnersRepository {

    private static final String MEDIA_OWNER_LOOKUP_QUERY = "select distinct media_owner from arunan_mediaowner_lookup";
	private static final Logger LOG = LoggerFactory.getLogger(CommercialMediaOwnersRepositoryImpl.class);
	private static final String MEDIA_OWNER_PRIORITIES = "MediaOwnerPriorities";

    @Autowired 
    CommercialInputStoreDao commercialInputStoreDao;
    
    @Autowired
    RedshiftConnectionProvider redshiftConnectionProvider;
    
    @Value("{$amnetdata.query.mediaowner}")
    private String mediaOwnerListFetchQuery;
    
    public String getMediaOwnerListFetchQuery() {
		return mediaOwnerListFetchQuery;
	}

	public void setMediaOwnerListFetchQuery(String mediaOwnerListFetchQuery) {
		this.mediaOwnerListFetchQuery = mediaOwnerListFetchQuery;
	}

	public String updateCommercialRecord(CommInputCreteriaImpl commInputCriteria) throws JsonProcessingException{
        CommercialInputStore commercialinputstore;
        ObjectMapper objectMapper = new ObjectMapper();
        String commInputToJson = null;
        Date updateDate = new Date();
            int id = commInputCriteria.getId();

            commercialinputstore = commercialInputStoreDao.findById(id);

            if (commercialinputstore == null) {
                LOG.error("Commercial input not found for id:: {}", id);
                throw new DataBaseAccessException("Error in updating record");
            } else {
                commercialinputstore.setMediaOwner(commInputCriteria.getDomain());
                commercialinputstore.setPriority(commInputCriteria.getPriority());
                commercialinputstore.setUpdateDate(updateDate);

                commercialInputStoreDao.save(commercialinputstore);
            }
            commInputToJson = objectMapper.writeValueAsString(commercialinputstore);
            LOG.info("updating record {} ::",commInputToJson);
        return commInputToJson;
    }

    @Override
    public DeleteMediaOwner deleteMediaOwner(int id) {
       
    	DeleteMediaOwnerDo deleteMediaOwnerDo = null;
       	CommercialInputStore commercialInputStoreRecordById = commercialInputStoreDao.findById(id);
        		
        	if(commercialInputStoreRecordById != null && commercialInputStoreRecordById.getId() != 0) {
        		deleteMediaOwnerDo=new DeleteMediaOwnerDo();
            	deleteMediaOwnerDo.setId(commercialInputStoreRecordById.getId());
            	deleteMediaOwnerDo.setDomain(commercialInputStoreRecordById.getMediaOwner());
            	deleteMediaOwnerDo.setPriority(commercialInputStoreRecordById.getPriority());
        		commercialInputStoreDao.deleteById(id);
        	}else {
        		    LOG.error("Record not found with required id", id);
        			throw new DataBaseAccessException("Record not found with required id");
        	}
        	return deleteMediaOwnerDo;
        } 
  

    @Override
    public MediaOwners saveMediaOwnersAndAssociatedPriorities(MediaOwners mediaOwnersAndAssociatedPrioritiesList,int profileId){
        LOG.debug("Inside Save File data containing Media Owners and Priorities");
        List<CommercialInputStore> commInputStoreList = new ArrayList<>();
    	try {
            commercialInputStoreDao.deleteAll();
            commInputStoreList = MediaOwnersTransformer.transform(mediaOwnersAndAssociatedPrioritiesList,profileId);
            commercialInputStoreDao.save(commInputStoreList);
        } catch (Exception e) {
            LOG.error("Problem in saving File data containing Media Owners and Priorities : {}",e.getMessage(),e);
            throw new DataBaseAccessException("Problem in uploading File data containing Media Owners and Priorities");
        }
        return mediaOwnersAndAssociatedPrioritiesList;
    }
    
    @Override
    public List<CommercialInputStore> getMediaOwnersAndAssociatedPriorities() {
        List<CommercialInputStore> commercialInputStoreList = null;
        try {
            commercialInputStoreList = commercialInputStoreDao.findAllByOrderByPriority();
        } catch (DataBaseAccessException e) {
            LOG.error("Error occured while fetching MediaOwner and Associated Priorities ",e.getMessage(),e);
            throw new DataBaseAccessException("Error occured while fetching MediaOwner and Associated Priorities");
        }
        return commercialInputStoreList;
    }
    
    @Override
    public List<MediaOwner> getDbmMediaOwnersFromRedshiftLookup() throws SQLException {
    	List<MediaOwner> mediaOwnersList = new ArrayList<>();
    	MediaOwnerDo mediaOwnersDo = null;
    	String mediaOwner = null;
    	Connection connection = redshiftConnectionProvider.getRedShiftConnection(MEDIA_OWNER_PRIORITIES);
        LOG.debug("Executing Media Owners query for REDSHIFT DATABASE {}",MEDIA_OWNER_LOOKUP_QUERY);
    	ResultSet result = RedshiftQueryExecutor.executeQuery(connection, MEDIA_OWNER_LOOKUP_QUERY);
        try {
		        while(result.next()) {
		        	mediaOwnersDo = new MediaOwnerDo();
		        	mediaOwner = result.getString("media_owner");
		        	mediaOwnersDo.setMediaOwner(mediaOwner);
		        	mediaOwnersDo.setPriority(0);
		        	mediaOwnersList.add(mediaOwnersDo);
		        	mediaOwner = null;
		        }
        }catch(SQLException sqlException) {
        	LOG.error("Exception occured while preparing Media Owner Object from resultset");
        }
        LOG.info("Media Owner list retrieved from Redshift with size is {} and data: {}",mediaOwnersList.size(),mediaOwnersList.toString());
        result.getStatement().close();
        connection.close();
        
        return mediaOwnersList;
    }
}