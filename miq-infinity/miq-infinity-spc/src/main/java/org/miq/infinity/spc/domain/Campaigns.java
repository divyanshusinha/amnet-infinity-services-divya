package org.miq.infinity.spc.domain;

import java.util.List;

public interface Campaigns {
	List<Campaign> getCampaigns();
}