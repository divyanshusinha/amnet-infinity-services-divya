package org.miq.infinity.spc.domain;

public interface InsertionOrder {
	int getIoId();
	String getIoName();
}
