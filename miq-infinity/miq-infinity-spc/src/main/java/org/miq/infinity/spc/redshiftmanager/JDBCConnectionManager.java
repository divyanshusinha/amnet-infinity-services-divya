package org.miq.infinity.spc.redshiftmanager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JDBCConnectionManager {
	private static final Logger LOG = LoggerFactory.getLogger(JDBCConnectionManager.class);
	private static Connection connection = null;
	
	public static Connection getConnection(String connectionURL, String username, String password ) {
		LOG.debug("Inside Connection Manager : Establishing connection Started");
		registerDriver();
		connection = createConnection(connectionURL, username, password);
		if(connection == null) {
			LOG.error("DB Connection could not be established");
		}else {
			LOG.debug("DB Connection Successfully established");
		}
		return connection;
	}

	private static Connection createConnection(String connectionURL, String username, String password) {
		try {
			  connection = DriverManager.getConnection(connectionURL,username,password);
		}catch(SQLException sqlException) {
			LOG.error("Exception occured while trying to establish db connection",sqlException.getMessage(),sqlException);
		}
		return connection;
	}

	private static void registerDriver() {
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			LOG.error("Could not locate Postgres Driver:",e);
		}
	}
}
