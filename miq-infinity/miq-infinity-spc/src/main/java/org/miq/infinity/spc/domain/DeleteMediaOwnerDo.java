package org.miq.infinity.spc.domain;

public class DeleteMediaOwnerDo implements DeleteMediaOwner {
	private int id;
    private int priority;
    private String domain;

    public void setId(int id) {
        this.id = id;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public int getPriority() {
        return priority;
    }

    @Override
    public String getDomain() {
        return domain;
    }

}
 
