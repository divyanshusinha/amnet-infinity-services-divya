package org.miq.infinity.spc.repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.miq.infinity.spc.domain.Advertiser;
import org.miq.infinity.spc.domain.AdvertiserDo;
import org.miq.infinity.spc.domain.Advertisers;
import org.miq.infinity.spc.domain.AdvertisersDo;
import org.miq.infinity.spc.domain.Campaign;
import org.miq.infinity.spc.domain.CampaignDo;
import org.miq.infinity.spc.domain.Campaigns;
import org.miq.infinity.spc.domain.CampaignsDo;
import org.miq.infinity.spc.domain.InsertionOrder;
import org.miq.infinity.spc.domain.InsertionOrderDo;
import org.miq.infinity.spc.domain.InsertionOrders;
import org.miq.infinity.spc.domain.InsertionOrdersDo;
import org.miq.infinity.spc.redshiftmanager.RedshiftConnectionProvider;
import org.miq.infinity.spc.redshiftmanager.RedshiftQueryExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class DbmAdvertiserIOCampaignRepositoryImpl implements DbmAdvertiserIOCampaignRepository{
	
	private static final String DBM_ADVERTISERS_LIST_QUERY = "select distinct advertiser_id,advertiser_name from lu_dbm_adv_io_placement";
	private static final String DBM_IO_BY_ADVERTISER_ID_QUERY = "select distinct io_id,io_name from lu_dbm_adv_io_placement where advertiser_id=";
	private static final String DBM_CAMPAIGNS_BY_INSERTION_ORDER_QUERY = "select distinct id,name from lu_dbm_adv_io_placement where io_id=";
	private static final Logger LOG = LoggerFactory.getLogger(DbmAdvertiserIOCampaignRepositoryImpl.class);
	private static final String DBM_ADVERTISERS = "DbmAdvertisers";
	
	private Connection connection;
	
	@Autowired
    RedshiftConnectionProvider redshiftConnectionProvider;
	
	@Override
	public Advertisers getDbmAdvertisers() {
		AdvertisersDo advertisersDo =  new AdvertisersDo();
		int advertiserId;
		String advertiserName = null;
		this.getDbmRedshiftConnection();
		Connection connectionRetrieved = this.getConnection();
		AdvertiserDo advertiserDo = null;
		List<Advertiser> advertisersList = new ArrayList<>();
		LOG.info("Connection for getDbmAdvertisers: {}",connectionRetrieved);
		LOG.info("Executing Dbm Advertisers query for REDSHIFT DATABASE {}",DBM_ADVERTISERS_LIST_QUERY);
		try {	
			ResultSet result = RedshiftQueryExecutor.executeQuery(connectionRetrieved, DBM_ADVERTISERS_LIST_QUERY);
	        while(result.next()) {
	        	advertiserDo = new AdvertiserDo();
	        	advertiserId = result.getInt("advertiser_id");
	        	advertiserDo.setAdvertiserId(advertiserId);
	        	advertiserName=result.getString("advertiser_name");
	        	advertiserDo.setAdvertiserName(advertiserName);
	        	advertisersList.add(advertiserDo);
	        }
		   }catch(SQLException sqlException) {
		    	LOG.error("Exception occured while preparing Media Owner Object from resultset");
		 }
        LOG.info("DBM Advertisers list retrieved from Redshift with size is {} and data {} ",advertisersList.size(),advertisersList.toString());
        
		advertisersDo.setAdvertisers(advertisersList);
		return advertisersDo;
	}

	@Override
	public InsertionOrders getDbmInsertionOrdersByAdvertiserId(int advertiserId) {
		InsertionOrdersDo insertionOrdersDo =  new InsertionOrdersDo();
		int ioId;
		String ioName = null;
		this.getDbmRedshiftConnection();
		Connection connectionRetrieved = this.getConnection();
		InsertionOrderDo insertionOrderDo = null;
		String queryToFetchInsertionOrders = null;
		List<InsertionOrder> insertionOrderList = new ArrayList<>();
		queryToFetchInsertionOrders=DBM_IO_BY_ADVERTISER_ID_QUERY+advertiserId;
		if(LOG.isDebugEnabled()) {
			LOG.debug("Connection for getDbmInsertionOrdersByAdvertiserId:{}",connectionRetrieved);
			LOG.debug("Executing Dbm Insertion Orders query for REDSHIFT DATABASE {}",queryToFetchInsertionOrders);
		}
		
		try {	
				ResultSet result = RedshiftQueryExecutor.executeQuery(connectionRetrieved, queryToFetchInsertionOrders);
				while(result.next()) {
		        	insertionOrderDo = new InsertionOrderDo();
		        	ioId = result.getInt("io_id");
		        	insertionOrderDo.setIoId(ioId);
		        	ioName=result.getString("io_name");
		        	insertionOrderDo.setIoName(ioName);
		        	insertionOrderList.add(insertionOrderDo);
		        }
				result.getStatement().close();
				this.closeConnection();
		}catch(SQLException sqlException) {
			LOG.error("Exception occured while preparing Insertion Order Object from resultset");
		}
		LOG.info("DBM insertion orders list retrieved from Redshift with size is {} and data {}",insertionOrderList.size(),insertionOrderList.toString());
		
		
		insertionOrdersDo.setInsertionOrders(insertionOrderList);
		return insertionOrdersDo;
	}

	@Override
	public Campaigns getDbmCampaignByInsertionOrderId(int insertionOrderId){
		CampaignsDo campaignsDo =  new CampaignsDo();
		int campaignId;
		String campaignName = null;
		this.getDbmRedshiftConnection();
		Connection connectionRetrieved = this.getConnection();
		CampaignDo campaignDo = null;
		String queryToFetchCampaigns = null;
		List<Campaign> campaignsList = new ArrayList<>();
		queryToFetchCampaigns=DBM_CAMPAIGNS_BY_INSERTION_ORDER_QUERY+insertionOrderId;
		if(LOG.isDebugEnabled()) {
			LOG.debug("Connection for getDbmCampaignByInsertionOrderId:{}",connectionRetrieved);
			LOG.debug("Executing DBM Campaign query for REDSHIFT DATABASE {}",queryToFetchCampaigns);
		}
		
		try {	
				ResultSet result = RedshiftQueryExecutor.executeQuery(connectionRetrieved, queryToFetchCampaigns);
		        while(result.next()) {
		        	campaignDo = new CampaignDo();
		        	campaignId = result.getInt("id");
		        	campaignDo.setCampaignId(campaignId);
		        	campaignName=result.getString("name");
		        	campaignDo.setCampaignName(campaignName);
		        	campaignsList.add(campaignDo);
		        }
		        result.getStatement().close();
				this.closeConnection();
		}catch(SQLException sqlException) {
			LOG.error("Exception occured while preparing Campaigns Object from resultset");
		}
		LOG.info("DBM insertion orders list retrieved from Redshift with size is {} and data {}",campaignsList.size(),campaignsList.toString());
		
		campaignsDo.setCampaigns(campaignsList);
		return campaignsDo;
	}
	
	private void getDbmRedshiftConnection() {
		Connection connectionEstablished = redshiftConnectionProvider.getRedShiftConnection(DBM_ADVERTISERS);
		this.setConnection(connectionEstablished);
	}
	
	

	public Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}
	
	private void closeConnection() throws SQLException {
		this.getConnection().close();
	}

}
