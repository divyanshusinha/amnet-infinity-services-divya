package org.miq.infinity.spc.engine.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.miq.infinity.spc.domain.SPCRecommendationsAndTradersInputDo;
import org.miq.infinity.spc.domain.TraderInputCriteriaImpl;
import org.miq.infinity.spc.engine.SpcProfileService;
import org.miq.infinity.spc.engine.SpcRecommendationOutputService;
import org.miq.infinity.spc.engine.SpcTraderService;
import org.miq.infinity.spc.repository.TraderInputRepository;
import org.miq.infinity.spc.util.SpcUtil;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration
public class SpcTraderServiceTest {

    @Mock
    private SpcProfileService spcProfileService;

    @Mock
    private SpcUtil spcUtil;

    @Mock
    TraderInputRepository traderInputRepository;

    @Mock
    SpcRecommendationOutputService spcRecommendationOutputService;
    
    @InjectMocks
    SpcTraderService spcTraderService;

    TraderInputCriteriaImpl traderInput;

    @Before
    public void init() {
        Date startDate = new Date();

        List<String> creativeSize = new ArrayList<>();
        creativeSize.add("300*500");
        List<String> deviceType = new ArrayList<>();
        deviceType.add("Desktop");
        
        traderInput = new TraderInputCriteriaImpl();
        traderInput.setProfileName("sunil.com");
        traderInput.setAdvertiserId(1234);
        traderInput.setBudget(9000);
        traderInput.setCampaignId(4567);
        traderInput.setCampaignType("Retargetting");
        traderInput.setCtrGoal("0.02");
        traderInput.setCreativeSize(creativeSize);
        //traderInput.setDeviceType(deviceType);
        traderInput.setImpressions(900000);
        traderInput.setStartdate(startDate);
        traderInput.setEnddate(startDate);

    }
 
    @Test
    public void uploadTraderSucessTest() throws IOException {
        String profileName = "sunil.com";
        int profileId = 1;
        String creativeSize = traderInput.getCreativeSize().toString();
        SPCRecommendationsAndTradersInputDo computeStatus=new SPCRecommendationsAndTradersInputDo();
        String uploadStatus = "Data saved successfully";

        Mockito.when(spcProfileService.getProfileId(profileName)).thenReturn(profileId);
        
        Mockito.when(spcRecommendationOutputService.generateSPCRecommendations(traderInput, profileId)).thenReturn(computeStatus);
        //Mockito.when(status1.equalsIgnoreCase("SUCCESS")).thenReturn(true);
     
        Mockito.when(traderInputRepository.saveTraderInput(traderInput, profileId)).thenReturn(uploadStatus);
        
        SPCRecommendationsAndTradersInputDo status=spcTraderService.processTraderInput(traderInput);
        
        System.out.println("Ststus is"+status);
        
        org.junit.Assert.assertEquals(uploadStatus,status);
    }
     
    @Test
    public void uploadTraderErrorTest() throws IOException {
        String profileName = "sunil.com"; 
        int profileId = 1;
        SPCRecommendationsAndTradersInputDo computeStatus=new SPCRecommendationsAndTradersInputDo();

        Mockito.when(spcProfileService.getProfileId(profileName)).thenReturn(profileId);
        
        Mockito.when(spcRecommendationOutputService.generateSPCRecommendations(traderInput, profileId)).thenReturn(computeStatus);
        
        //Mockito.when(traderInputRepository.saveTraderInput(traderInput, profileId, creativeSize)).thenReturn(status);
        
        SPCRecommendationsAndTradersInputDo status=spcTraderService.processTraderInput(traderInput);
        
        org.junit.Assert.assertEquals(computeStatus,status);
    }
}
