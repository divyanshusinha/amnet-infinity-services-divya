package org.miq.infinity.spc.engine.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.miq.infinity.spc.domain.CommInputCreteriaImpl;
import org.miq.infinity.spc.domain.DeleteMediaOwner;
import org.miq.infinity.spc.domain.DeleteMediaOwnerDo;
import org.miq.infinity.spc.domain.MediaOwner;
import org.miq.infinity.spc.domain.MediaOwnerDo;
import org.miq.infinity.spc.domain.MediaOwners;
import org.miq.infinity.spc.domain.MediaOwnersDo;
import org.miq.infinity.spc.engine.CommercialMediaOwnersService;
import org.miq.infinity.spc.engine.SpcProfileService;
import org.miq.infinity.spc.entity.CommercialInputStore;
import org.miq.infinity.spc.exception.ResourceNotFoundException;
import org.miq.infinity.spc.repository.CommercialMediaOwnersRepository;
import org.miq.infinity.spc.util.SpcUtil;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration
public class CommercialMediaOwnersServiceTest {

    @Mock
    private SpcProfileService spcProfileService;

    @Mock
    private SpcUtil spcUtil;
 
    @Mock
    CommercialMediaOwnersRepository commercialMediaOwnersRepository;

    @InjectMocks
    CommercialMediaOwnersService spc;

    List<CommercialInputStore> commList1 = null;
    List<MediaOwner> mediaOwnerList= null;
    MediaOwnersDo mediaOwnersDo = null;
    @Before
    public void init() {

        commList1 = new ArrayList<CommercialInputStore>();
        mediaOwnerList = new ArrayList<>();
        CommercialInputStore commInput = new CommercialInputStore();
        MediaOwnerDo mediaOwnerDo = new MediaOwnerDo();
        mediaOwnerDo.setMediaOwner("Yahoo");
        mediaOwnerDo.setPriority(1);
        mediaOwnerList.add(mediaOwnerDo);
        Date createDate = new Date();
        commInput.setMediaOwner("Test.com");
        commInput.setPriority(1);
        commInput.setProfileId(10);
        commInput.setCreateDate(createDate);
        commInput.setUpdateDate(createDate);
        commInput.setIsActive(1);

        commList1.add(commInput);
        
        mediaOwnersDo =new MediaOwnersDo();
        mediaOwnersDo.setMediaOwners(mediaOwnerList);
    }

    // Checking for getting commercial input list is not null
    @Test 
    public void spcListNotNullExampleTest() {
        List<CommercialInputStore> commList1 = new ArrayList<CommercialInputStore>();
        CommercialInputStore commInput = new CommercialInputStore();

        Date createDate = new Date();
        commInput.setMediaOwner("Test.com");
        commInput.setPriority(1);
        commInput.setProfileId(10);
        commInput.setCreateDate(createDate);
        commInput.setUpdateDate(createDate);
        commInput.setIsActive(1); 

        commList1.add(commInput);

        Mockito.when(commercialMediaOwnersRepository.getMediaOwnersAndAssociatedPriorities()).thenReturn(commList1);

        List<CommercialInputStore> commList2 = spc.getMediaOwnersAndAssociatedPriorities();

        org.junit.Assert.assertNotNull(commList2);

        System.out.println("Listing output is" + commList2.toString());
    }

    @Test(expected = ResourceNotFoundException.class)
    public void spcListExceptionExampleTest() {

        List<CommercialInputStore> commList = null;
        Mockito.when(commercialMediaOwnersRepository.getMediaOwnersAndAssociatedPriorities()).thenReturn(commList);

        Mockito.when(spcUtil.isCollectionEmpty(commList)).thenReturn(true);

        Mockito.when(commercialMediaOwnersRepository.getMediaOwnersAndAssociatedPriorities())
                .thenThrow(new ResourceNotFoundException("Commercial list not found"));
         spc.getMediaOwnersAndAssociatedPriorities();
    }

    @Test
    public void FileUploadTestSuccess() throws Exception {
        String profileName = "sunil.com";
        int profileId = 1;
        MockMultipartFile uploadfile = new MockMultipartFile("Commfile", "testCommInput.xlsx", "text/plain",
                "Spring Framework".getBytes());
        Mockito.when(spcProfileService.getProfileId(profileName)).thenReturn(profileId);
        Mockito.when(spcUtil.parseFileAndreturnCommercialInputsList(uploadfile, profileId)).thenReturn(mediaOwnersDo);
        Mockito.when(spcUtil.isCollectionEmpty(commList1)).thenReturn(false);
        Mockito.when(commercialMediaOwnersRepository.saveMediaOwnersAndAssociatedPriorities(mediaOwnersDo,1)).thenReturn(mediaOwnersDo);
        MediaOwners returnedData = spc.uploadMediaOwnersCommercialFileData(uploadfile, profileName);
        org.junit.Assert.assertEquals(returnedData, mediaOwnersDo);
    }

//    @Test(expected = BadRequestException.class)
//    public void FileUploadTestFail() throws Exception {
//        String profileName = "sunil.com";
//        int profileId = 1;
//        //String failStatus = "Error in uploading and saving file";
//
//        MockMultipartFile uploadfile = new MockMultipartFile("Commfile", "testCommInput.xlsx", "text/plain",
//                "Spring Framework".getBytes());
//
//        Mockito.when(spcProfileService.getProfileId(profileName)).thenReturn(profileId);
//
//        Mockito.when(spcUtil.parseFileAndreturnCommercialInputsList(uploadfile, profileId)).thenReturn(mediaOwnersDo);
//
//        Mockito.when(spcUtil.isCollectionEmpty(commList1)).thenReturn(true);
//        
//        spc.uploadMediaOwnersCommercialFileData(uploadfile, profileName);
//    }
    
    @Test
    public void commInputDeleteTestSuccess() {
        int id = 1;

        DeleteMediaOwnerDo deletionMessage = new DeleteMediaOwnerDo();

        Mockito.when(commercialMediaOwnersRepository.deleteMediaOwner(id)).thenReturn(deletionMessage);

        DeleteMediaOwner status = spc.deleteMediaOwner(id);

        org.junit.Assert.assertEquals(deletionMessage, status);
    }

    @Test
    public void commInputUpdateTest() throws IOException {

        ObjectMapper objectMapper = new ObjectMapper();

        Date updationDate = new Date();

        CommInputCreteriaImpl commInputCriteria = new CommInputCreteriaImpl();
        commInputCriteria.setDomain("sunil.com");
        commInputCriteria.setPriority(2);
        commInputCriteria.setId(1);

        CommercialInputStore updatedOutput = new CommercialInputStore();

        updatedOutput.setId(1);
        updatedOutput.setMediaOwner("sunil.com");
        updatedOutput.setUpdateDate(updationDate);

        String JsonUpdatedOutput = objectMapper.writeValueAsString(updatedOutput);

        Mockito.when(commercialMediaOwnersRepository.updateCommercialRecord(commInputCriteria)).thenReturn(JsonUpdatedOutput);

        spc.updateCommercialInput(commInputCriteria);

        org.junit.Assert.assertNotNull(JsonUpdatedOutput);

    }
}
