/*
 * Author : Subhan Ansari
 */
package com.miq.infinity.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class RestDispatcher {

	private static final Logger LOG = LoggerFactory.getLogger(RestDispatcher.class);
	public static final String SERVICE_VERSION_1 = "/service/v1";
	
	public void RestDispatcherPurpose() {
		LOG.info("Maintaining version of API's");
	}
}
